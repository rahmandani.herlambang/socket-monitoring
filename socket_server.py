import socket
import argparse

#*******************************************************************************************************88
parser = argparse.ArgumentParser()
parser.add_argument("--host", "-o", help="set host")
parser.add_argument("--port", "-p", help="set port")

# read arguments from the command line
args = parser.parse_args()

# #*******************************************************************************************************88
s = socket.socket()
host = args.host
port = int(args.port)
s.bind((host, port))

s.listen(5)
while True:
    clientsocket, addr = s.accept() # menerima permintaan dari client
    # print ('Mendapatkan permintaan koneksi dari', addr) # cetak pesan yang menunjukkan alamat IP dari client

    clientsocket.send('OK!'.encode()) # kirim pesan ke client
    data_client = clientsocket.recv(2000).decode() #data yang dikirimkan client
    print (data_client)

    clientsocket.close() # menutup koneksi
# #*******************************************************************************************************88
